#!/bin/ksh
#
#        kkkkkkkk                         hhhhhhh
#        k::::::k                         h:::::h
#        k::::::k                         h:::::h
#        k::::::k                         h:::::h
#        k:::::k    kkkkkkk  ssssssssss   h::::h hhhhh
#        k:::::k   k:::::k ss::::::::::s  h::::hh:::::hhh
#        k:::::k  k:::::kss:::::::::::::s h::::::::::::::hh
#        k:::::k k:::::k s::::::ssss:::::sh:::::::hhh::::::h
#        k::::::k:::::k   s:::::s  ssssss h::::::h   h::::::h
#        k:::::::::::k      s::::::s      h:::::h     h:::::h
#        k:::::::::::k         s::::::s   h:::::h     h:::::h
#        k::::::k:::::k  ssssss   s:::::s h:::::h     h:::::h
#        k::::::k k:::::ks:::::ssss::::::sh:::::h     h:::::h
#        k::::::k  k:::::ks::::::::::::::sh:::::h     h:::::h
#        k::::::k   k:::::ks:::::::::::ss h:::::h     h:::::h
#        kkkkkkkk    kkkkkkksssssssssss   hhhhhhh     hhhhhhh
#
##########################################################################

# If not running interactively, don't do anything.
[[ $- != *i* ]] && return

# source wal colors
# (cat /home/mitch/.cache/wal/sequences &)

# remove junk
(rm -rf ${HOME}/*.core ${HOME}/Desktop ${HOME}/Downloads \
    ${HOME}/nohup.out ${HOME}/*.hup > /dev/null 2>&1 &)

# source my aliases
. ${HOME}/.aliases

# ksh-specific aliases:
alias echo="builtin print" # print is a better 'echo'

export HISTFILE="${HOME}/.ksh_history"
export HISTSIZE=100000000 # probably should reduce this
export SAVEHIST=$HISTSIZE
export HISTCONTROL=ignoreboth

# disable core file generation
ulimit -c 0

set -o vi-tabcomplete
set bgnice # run background processes with lower prio
set nounset # disable usage of 'set'
bind Space:magic-space > /dev/null
