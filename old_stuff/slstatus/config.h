static const int interval = 500;
#define MAXLEN 256

// different separators:
/* ∙ */

static const struct arg args[] = {
    
    { run_command, "%s", "/home/mitch/bin/get-song.sh", 3 },
    { run_command, " %s%%", "sh /home/mitch/bin/battery-check.sh", 30 },
    { run_command, " %s  ∙", "sh /home/mitch/bin/battery-time.sh", 30 },

    // if the device doesn't exist [ if disabled, or really doesn't exist ],
    // this script will spit out errors. Silence that to /dev/null.
    { run_command, " %s ∙", "sh /home/mitch/bin/wifi-check.sh iwn0 2> /dev/null", 6 },

    { run_command, " %s ∙", "sh /home/mitch/bin/vpn-check.sh", 6 },

    /* { run_command, " %s /", "bash /home/mitch/bin/BSDNixCPUPercent.sh"  }, */
    { run_command, " %s ∙", "sh /home/mitch/bin/BSDNixCPUTemp.sh", 10 },

    { run_command, " %s%% ∙", "sh /home/mitch/bin/BSDNixVolume.sh -get", 1 },

	{ datetime,    " %s ",    "%a %b %d - %H:%M", 115 },

};
