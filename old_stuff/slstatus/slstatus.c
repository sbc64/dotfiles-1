#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <X11/Xlib.h>

#include "arg.h"
#include "slstatus.h"
#include "util.h"

struct arg {
	const char *(*func)();
	const char *fmt;
	const char *args;
    const int timer;
    char storage[50];
};

char *argv0;
char buf[1024];
static Display *dpy;

#include "config.h"

static void difftimespec(struct timespec *res, struct timespec *a, struct timespec *b) {
	res->tv_sec = a->tv_sec - b->tv_sec - (a->tv_nsec < b->tv_nsec);
	res->tv_nsec = a->tv_nsec - b->tv_nsec +
	               (a->tv_nsec < b->tv_nsec) * 1000000000;
}

int main() {
	struct timespec start, current, diff, intspec, wait;
	size_t i, len;
	char status[MAXLEN];
	dpy = XOpenDisplay(NULL);

    int timer = 0;
	while (1) {
		clock_gettime(CLOCK_MONOTONIC, &start);

		for (i = len = 0; i < LEN(args); i++) {
            if(timer % args[i].timer == 0)
                snprintf(args[i].storage, 50, args[i].func(args[i].args));
            len += snprintf(status + len, sizeof(status) - len,
                        args[i].fmt, args[i].storage);
		}

        XStoreName(dpy, DefaultRootWindow(dpy), status);
        XSync(dpy, False);

        clock_gettime(CLOCK_MONOTONIC, &current);
        difftimespec(&diff, &current, &start);

        intspec.tv_sec = interval / 1000;
        intspec.tv_nsec = (interval % 1000) * 1000000;
        difftimespec(&wait, &intspec, &diff);

        if (wait.tv_sec >= 0) nanosleep(&wait, NULL);
        timer++;
	}

	return 0;
}
