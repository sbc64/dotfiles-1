void init_dwm_info(const int gappx, const int BAR_HEIGHT, const int topbar) {
    system("rm -rf /tmp/dwm_info");
    mkdir("/tmp/dwm_info", 0777);

    FILE *f1 = fopen("/tmp/dwm_info/ws1", "w"); fprintf(f1, "0"); fclose(f1);
    FILE *f2 = fopen("/tmp/dwm_info/ws2", "w"); fprintf(f2, "0"); fclose(f2);
    FILE *f3 = fopen("/tmp/dwm_info/ws3", "w"); fprintf(f3, "0"); fclose(f3);
    FILE *f4 = fopen("/tmp/dwm_info/ws4", "w"); fprintf(f4, "0"); fclose(f4);
    FILE *f5 = fopen("/tmp/dwm_info/ws5", "w"); fprintf(f5, "0"); fclose(f5);
    FILE *f6 = fopen("/tmp/dwm_info/ws6", "w"); fprintf(f6, "0"); fclose(f6);
    /* FILE *f7 = fopen("/tmp/dwm_info/ws7", "w"); fprintf(f7, "0"); fclose(f7); */
    /* FILE *f8 = fopen("/tmp/dwm_info/ws8", "w"); fprintf(f8, "0"); fclose(f8); */
    FILE *fCWS = fopen("/tmp/dwm_info/current_ws", "w"); fprintf(fCWS, "1"); fclose(fCWS);
    FILE *fCL = fopen("/tmp/dwm_info/current_layout", "w"); fprintf(fCL, "0"); fclose(fCL);
    FILE *fCHECK = fopen("/tmp/dwm_info/check", "w"); fprintf(fCHECK, "1"); fclose(fCHECK);

    // store information about dwm
    FILE *fgappx = fopen("/tmp/dwm_info/gappx", "w"); fprintf(fgappx, "%d", gappx); fclose(fgappx);
    FILE *fbar_height = fopen("/tmp/dwm_info/bar_height", "w"); fprintf(fgappx, "%d", BAR_HEIGHT); fclose(fbar_height);
    FILE *ftop_bar = fopen("/tmp/dwm_info/top_bar", "w"); fprintf(fgappx, "%d", topbar); fclose(ftop_bar);
}

void set_dwm_info_current_workspace(int i) {
    FILE *f = fopen("/tmp/dwm_info/current_ws", "w");
    fprintf(f, "%d", i);
    fclose(f);
    FILE *fCHECK = fopen("/tmp/dwm_info/check", "w"); fprintf(fCHECK, "1"); fclose(fCHECK);
}
void set_dwm_info_current_layout(int i) {
    // sets index of current layout in layouts[]
    FILE *f = fopen("/tmp/dwm_info/current_layout", "w");
    fprintf(f, "%d", i);
    fclose(f);
    FILE *fCHECK = fopen("/tmp/dwm_info/check", "w"); fprintf(fCHECK, "1"); fclose(fCHECK);
}

void toggle_dwm_info_ws(int ws, int bool) {
    // toggles whether the given tag has clients or not 
    // NOTE: bool should only be either '1' or 0'
    FILE *f = NULL;
    if(ws == 1) {
        f = fopen("/tmp/dwm_info/ws1", "w");
        fprintf(f, "%d", bool);
    } else if(ws == 2) {
        f = fopen("/tmp/dwm_info/ws2", "w");
        fprintf(f, "%d", bool);
    } else if(ws == 3) {
        f = fopen("/tmp/dwm_info/ws3", "w");
        fprintf(f, "%d", bool);
    } else if(ws == 4) {
        f = fopen("/tmp/dwm_info/ws4", "w");
        fprintf(f, "%d", bool);
    } else if(ws == 5) {
        f = fopen("/tmp/dwm_info/ws5", "w");
        fprintf(f, "%d", bool);
    } else if(ws == 6) {
        f = fopen("/tmp/dwm_info/ws6", "w");
        fprintf(f, "%d", bool);
    } 
    /* else if(ws == 7) { */
    /*     f = fopen("/tmp/dwm_info/ws7", "w"); */
    /*     fprintf(f, "%d", bool); */
    /* } else if(ws == 8) { */
    /*     f = fopen("/tmp/dwm_info/ws8", "w"); */
    /*     fprintf(f, "%d", bool); */
    /* } */

    if(f) fclose(f);

    FILE *fCHECK = fopen("/tmp/dwm_info/check", "w"); fprintf(fCHECK, "1"); fclose(fCHECK);
}
