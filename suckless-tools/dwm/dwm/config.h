/* static const int gappx      = 40; /1* gap pixel between windows *1/ */
/* static const int gappx      = 28; /1* gap pixel between windows *1/ */
static const int gappx      = 24; /* gap pixel between windows */

// ------------------------------------------------------ //
/* static const int CORNER_RADIUS = 0; */
/* static const int BORDER_PX = 6; */
/* static const int round_non_floating = 0; // whether to round corners of tiled clients */
// ------------------------------------------------------ //

// ------------------------------------------------------ //
/* static const int BORDER_PX = 0; */
/* static const int CORNER_RADIUS = 14; */
/* static const int round_non_floating = 1; // whether to round corners of tiled clients */
// ------------------------------------------------------ //

// ------------------------------------------------------ //
static const int BORDER_PX = 6;
static const int CORNER_RADIUS = 14;
static const int round_non_floating = 0; // whether to round corners of tiled clients
// ------------------------------------------------------ //

static const int snap = 8;
static const int topbar        = 0; /* 0 means bottom bar */
static const int BAR_HEIGHT    = 68; // in pixels

static const char terminal[] = "st";

// whether to keep the border for terminals if
// they are the only window on the tag
// --- normally the borders would be hidden
static const int terminals_keep_border = 0;

/* #include "normal-theme.h" */
#include "/home/mitch/.cache/wal/colors-wal-dwm.h"

static const int NUM_WORKSPACES=6;
static const char *tags[] = { " ONE ", " TWO ", " THREE ", " FOUR ", " FIVE ", " SIX " };

static const Rule rules[] = {
    /* class      instance    title                 tags mask  iscentered   isfloating   monitor */
    /* { "Gimp",     NULL,       "Gimp",                 0,         0,           1,           -1 }, */
    { "Image Manipulation Program",     NULL,       "Image Manipulation Program",                 0,         0,           1,           -1 },
    
    // surf-download and surf-translate
    { "st",       NULL,       "surf-download",      0,         1,           1,           -1 },
    { "st",       NULL,       "surf-translate",     0,         1,           1,           -1 },
    
    { "feh",       NULL,       "feh",               0,         1,           1,           -1 },
    { "mpv",       NULL,       "mpv",               0,         1,           1,           -1 },
};

#include "tile.c"
#include "monocle.c"
#include "grid.c"
#include "horizgrid.c"
#include "fibonacci.c"
#include "centeredmaster.c"
#include "centeredfloatingmaster.c"
static const int NUM_LAYOUTS = 8;
static const Layout layouts[] = {
    /* symbol     arrange function          keybind */
    { "[T]",      tile },                   // mod-t
    { "[F]",      NULL },                   // mod-f 
    { "[M]",      monocle },                // mod-m
    { "[GGG]",    grid },                   // mod-g
    { "[MMM]",    centeredmaster },         // mod-n
    { "[CFM]",    centeredfloatingmaster }, // mod-b
    { "[FFF]",    dwindle },                // mod-v
    { "[HHH]",    horizgrid },              // mod-d
};

#define TAGKEYS(KEY,TAG) \
{ Mod1Mask,                       KEY,      view,           {.ui = 1 << TAG} }, \
{ Mod1Mask|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
{ Mod1Mask|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
{ Mod1Mask|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

static char dmenumon[2] = "0";
static const char *dmenucmd[] = { "dash", "-c", "${HOME}/bin/dmenu_run.sh", NULL };

static const char *net[] = { "dash", "-c", "uclimit -c 0  ;  tabbed -c surf -e", NULL };
static const char *term[]  = { "tabbed", "-c", "-r", "2", "st", "-w", "''", NULL };
/* static const char *term[]  = { "st", NULL }; */
static const char *ranger[] = { "st", "-e", "ranger", NULL };
static const char *vimwiki[] = { "st", "-w", "Vimwiki", "-T", "Vimwiki", "-n", "Vimwiki", "-e", "nvim", "-c VimwikiIndex", NULL };
static const char *musicplayer[] = { "st", "-w", "music", "-T", "music", "-n", "music", "-e", "ranger", "/home/mitch/music", NULL };
static const char *clipboard[] = { "miniclip", NULL };
static const char *dedit[] = { "dedit", NULL };
static const char *tasks[] = { "tasks", NULL };

static const char *volup[] = { "dash", "-c", "${HOME}/.dotfiles/suckless-tools/dwm/scripts-dwm/dwm-volume-control.sh --raise 9", NULL };
static const char *voldown[] = { "dash", "-c", "${HOME}/.dotfiles/suckless-tools/dwm/scripts-dwm/dwm-volume-control.sh --lower 9", NULL };
// --- for some reason this doesn't work on bsd
/* static const char *volmute[] = { "dash", "-c", "${HOME}/.dotfiles/suckless-tools/dwm/scripts-dwm/dwm-volume-control.sh --mute", NULL }; */

static const char *mpdnext[] = { "dash", "-c", "/usr/local/bin/mpvc --track 1", NULL };
static const char *mpdprev[] = { "dash", "-c", "/usr/local/bin/mpvc --track -1", NULL };
static const char *mpdtoggle[] = { "dash", "-c", "/usr/local/bin/mpvc --toggle", NULL };
static const char *mpdseekff[] = { "dash", "-c", "/usr/local/bin/mpvc --seek +30", NULL };
static const char *mpdseekrw[] = { "dash", "-c", "/usr/local/bin/mpvc --seek -30", NULL };
static const char *togglekeyboardlayout[] = { "dash", "-c", "${HOME}/bin/toggle-keyboard-layout.sh", NULL };
static const char *slock[] = { "slock", NULL };
static const char *scrot[] = { "scrot", NULL };

#include "movestack.c"
/* #define SH(cmd) { .v = (const char*[]){ "/bin/dash", "-c", cmd, NULL } } */
static Key keys[] = {
	/* modifier                     key        function        argument */
    // ------------------------------------------------------------------- //
    { Mod1Mask,                     XK_p,             spawn,    {.v = dmenucmd } },
    { Mod1Mask,                     XK_Return,        spawn,    {.v = term } },
    { Mod1Mask,                     XK_w,             spawn,    {.v = net } },
    { Mod1Mask,                     XK_o,             spawn,    {.v = dedit } },
    { Mod1Mask,                     XK_i,             spawn,    {.v = tasks } },
    { Mod1Mask,                     XK_c,             spawn,    {.v = clipboard } },
    { Mod1Mask,                     XK_r,             spawn,    {.v = ranger } },

    { Mod1Mask,                     XK_e,             spawn,    {.v = vimwiki } },
    { Mod1Mask,                     XK_a,             spawn,    {.v = musicplayer } },

    { Mod1Mask,                     XK_slash,         spawn,    {.v = mpdtoggle } },
    { Mod1Mask,                     XK_period,        spawn,    {.v = mpdnext } },
    { Mod1Mask,                     XK_comma,         spawn,    {.v = mpdprev } },
    { 0,                            XK_Print,         spawn,    {.v = scrot } },
    { Mod1Mask,                     XK_semicolon,     spawn,    {.v = voldown }},
    { Mod1Mask,                     XK_apostrophe,    spawn,    {.v = volup }},

    { Mod1Mask,                     XK_bracketleft,   spawn,    {.v = mpdseekrw }},
    { Mod1Mask,                     XK_bracketright,  spawn,    {.v = mpdseekff }},

    // xf86 keys must be in octal
    /* { 0,                        0x1008ff12, spawn,          {.v = volmute }}, */
    { 0,                        0x1008ff11, spawn,          {.v = voldown }},
    { 0,                        0x1008ff13, spawn,          {.v = volup }},

    { 0,                        0x1008ff17, spawn,          {.v = mpdnext } },
    { 0,                        0x1008ff16, spawn,          {.v = mpdprev } },
    { 0,                        0x1008ff14, spawn,          {.v = mpdtoggle } },

    /* { 0,                        0x1008ff2a, spawn,          {.v = slock }}, */
    { Mod1Mask,                     XK_x,       spawn,          {.v = slock }},
    { Mod1Mask|ControlMask,         XK_k,       spawn,          {.v = togglekeyboardlayout }},
    // ------------------------------------------------------------------ //
    { Mod1Mask,                     XK_j,       focusstack,     {.i = +1 } },
    { Mod1Mask,                     XK_k,       focusstack,     {.i = -1 } },
    { Mod1Mask,                     XK_l,       movestack,      {.i = +1 } },
    { Mod1Mask,                     XK_h,       movestack,      {.i = -1 } },
	{ Mod1Mask,                     XK_y,      incnmaster,     {.i = +1 } },
	{ Mod1Mask,                     XK_u,      incnmaster,     {.i = -1 } },
    // ------------------------------------------------------------------- //
    { Mod1Mask|ShiftMask,           XK_h,       setmfact,       {.f = -0.05} },
    { Mod1Mask|ShiftMask,           XK_l,       setmfact,       {.f = +0.05} },
    { Mod1Mask|ShiftMask,           XK_k,       setsmfact,      {.f = +0.05} },
    { Mod1Mask|ShiftMask,           XK_j,       setsmfact,      {.f = -0.05} },
    // ------------------------------------------------------------------- //
    { Mod1Mask,                     XK_Tab,     view,           {0} },
    { Mod1Mask|ShiftMask,           XK_space,   togglefloating, {0} },
	{ Mod1Mask,                     XK_s,       togglesticky,   {0} },
    // ------------------------------------------------------------------- //
    { Mod1Mask,                     XK_t,       setlayout,  {.v = &layouts[0]} }, // tiled
    { Mod1Mask,                     XK_f,       setlayout,  {.v = &layouts[1]} }, // floating
    { Mod1Mask,                     XK_m,       setlayout,  {.v = &layouts[2]} }, // monocle
    { Mod1Mask,                     XK_g,       setlayout,  {.v = &layouts[3]} }, // grid
    { Mod1Mask,                     XK_n,       setlayout,  {.v = &layouts[4]} }, // centered master
    { Mod1Mask,                     XK_b,       setlayout,  {.v = &layouts[5]} }, // centered floating master
    { Mod1Mask,                     XK_v,       setlayout,  {.v = &layouts[6]} }, // fibonacci
    { Mod1Mask,                     XK_d,       setlayout,  {.v = &layouts[7]} }, // horizgrid
    { Mod1Mask,                     XK_space,   setlayout,  {0} }, // toggle between last 2 layouts
    // ------------------------------------------------------------------- //
    { Mod1Mask,                     XK_q,       killclient, {0} },
    { Mod1Mask|ShiftMask|ControlMask,           XK_q,       quit,       {0} },
    // ------------------------------------------------------------------- //
    /* { Mod1Mask,                 XK_0,       view,           {.ui = ~0 } }, */
    /* { Mod1Mask|ShiftMask,           XK_0,       tag,            {.ui = ~0 } }, */
    // ------------------------------------------------------------------- //
    TAGKEYS(XK_1,0) TAGKEYS(XK_2,1) TAGKEYS(XK_3,2) TAGKEYS(XK_4,3)
	TAGKEYS(XK_5,4) TAGKEYS(XK_6,5) TAGKEYS(XK_7,6) TAGKEYS(XK_8,7)

    // ------------- musl-c does not like these for some reason ------------------------- 
    TAGKEYS(XK_9, (NUM_WORKSPACES - 5)) TAGKEYS(XK_0, (NUM_WORKSPACES - 4)) TAGKEYS(XK_minus,(NUM_WORKSPACES - 3)) TAGKEYS(XK_equal,(NUM_WORKSPACES - 2))
    TAGKEYS(XK_BackSpace,(NUM_WORKSPACES - 1))
};

static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkClientWin,         Mod1Mask,           Button1,        movemouse,      {0} },
	{ ClkClientWin,         Mod1Mask,           Button3,        resizemouse,    {0} },
	/* { ClkRootWin,         Mod1Mask,           Button3,        resizemouse,    {0} }, */
};
