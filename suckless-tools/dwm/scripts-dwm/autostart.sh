#!/bin/dash

if [ "$(uname)" != "Linux" ] ; then
    # disable core file generation
    ulimit -c 0
fi

xmodmap ~/.Xmodmap &

# in case dwm was restarted, kill all programs
pkill -9 unclutter bar bash sleep lemonbar clipmenud compton 2>&1 /dev/null

compton --config ${HOME}/.config/compton.conf -b > /dev/null &

# based on what is set as my wallpaper,
# this could either be a still picture
# or a cinemagraph. Find out what it is,
# and launch with the appropriate program.
feh="feh --bg-fill ${HOME}/.wall &"
mpvbg="mpvbg ${HOME}/.wall &"
case "$(file -b -i -L ${HOME}/.wall)" in
    "image/png") $feh & ;;
    "image/jpg") $feh & ;;
    "image/jpeg") $feh & ;;

    "image/gif") $feh ; $mpvbg & ;;
    "video/webm") $feh ; $mpvbg & ;;
    "video/mp4") $feh ; $mpvbg & ;;
    "video/flv") $feh ; $mpvbg & ;;
    "video/mkv") $feh ; $mpvbg & ;;
esac &


bar &

xset +fp ${HOME}/.fonts &
xrdb load ${HOME}/.Xresources &

xset m 0 0 &
unclutter -jitter 2 -noevents -idle 5 &
# For some bizarre reason, '-root' stops `tabbed`
# from starting. Why is this? May be a openbsd thing?
# unclutter -jitter 1 -root -noevents -idle 5

nohup miniclip --daemon -- > /dev/null 2>&1

# xautolock -time 10 -secure -locker /usr/local/bin/slock
# xbacklight -set 95
