/* Highest priority value will be used.
 * Default parameters are priority 0
 * Per-uri parameters are priority 1
 * Command parameters are priority 2 */
static Parameter defconfig[ParameterLast] = {
    [FontSize]            =       { { .i = 12 },    },
	[ZoomLevel]           =       { { .f = 1.2 },   },
	[DNSPrefetch]         =       { { .i = 1 },     },
	[JavaScript]          =       { { .i = 1 },     },
	[StrictTLS]           =       { { .i = 1 },     },
	[Style]               =       { { .i = 1 },     },
	[Images]              =       { { .i = 1 },     },
}; /* -------------------------------------------------------------------------- */

static int winsize[] = { 1280, 800 };
static char *fulluseragent  = "Mozilla/5.0";
static WebKitFindOptions findopts = WEBKIT_FIND_OPTIONS_CASE_INSENSITIVE | WEBKIT_FIND_OPTIONS_WRAP_AROUND;

#define SETPROP(r, s, p) { \
    .v = (const char *[]){ "/bin/sh", "-c", \
        "if [ $(pgrep dmenu) ] ; then exit ; else " \
        ". ${HOME}/.cache/wal/colors.sh ; prop=\"$(printf '%b' \"$(xprop -id $1 $2 " \
        "| sed \"s/^$2(STRING) = //;s/^\\\"\\(.*\\)\\\"$/\\1/\" && cat ~/.surf/bookmarks)\" " \
        "| dmenu -nb $color0 -nf $color15 -sb $color2 -sf $color15 -l 10 -p \"$4\" -w $1)\" && " \
        "xprop -id $1 -f $3 8u -set $3 \"$prop\"  ; fi", \
        "surf-setprop", winid, r, s, p, NULL \
    } \
}

#define SETSEARCH(r, s, p) { \
    .v = (const char *[]){ "/bin/sh", "-c", \
            ". ${HOME}/.cache/wal/colors.sh ; prop=\"$(printf '%b' \"$(xprop -id $1 $2 " \
            "| sed \"s/^$2(STRING) = //;s/^\\\"\\(.*\\)\\\"$/\\1/\")\" " \
            "| dmenu -nb $color0 -nf $color15 -sb $color2 -sf $color15 -p \"$4\" -w $1)\" && xprop -id $1 -f $3 8s -set $3 \"$prop\"", \
            "surf-setprop", winid, r, s, p, NULL \
    } \
}
