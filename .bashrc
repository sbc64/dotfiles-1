#!/bin/bash

# Note to those looking, I don't use bash anymore.
# Please see .kshrc and .aliases

# (cat /home/mitch/.cache/wal/sequences &)

(rm -rf /home/mitch/*.core > /dev/null &)

source ${HOME}/.aliases


set -o vi
